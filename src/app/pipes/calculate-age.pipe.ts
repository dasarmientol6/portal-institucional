import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'calculateAge'
})
export class CalculateAgePipe implements PipeTransform {

  /**
   * Filtro que permite calcular la edad cumplica hasta el 2021 de una persona
   *
   * @param yearOfBirth Anio de nacimiento
   */
  transform(yearOfBirth: string): string {

    // Si no hay un anio de nacimiento, se retorna vacio
    if (yearOfBirth === '' || !yearOfBirth) {
      return '';
    }

    // Se retorna el calculo de la edad cumplida solo por el anio
    return (2021 - Number(yearOfBirth)).toString();
  }

}
