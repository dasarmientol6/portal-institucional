import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Person } from '../../entities/person';
import { PersonagesService } from '../../services/personages.service';

@Component({
  selector: 'app-personages',
  templateUrl: './personages.component.html',
  styleUrls: ['./personages.component.scss']
})
export class PersonagesComponent implements OnInit {

  /*
  Arreglo que contiene los personajes
  */
  public personages: Array<Person>;

  /*
  Arreglo para indicar las columnas de la tabla de la interfaz
  */
  public displayedColumns: string[];

  /*
  Objeto que contiene la informacion de datos y configuracion de la tabla
  */
  public dataSource: MatTableDataSource<Person>;

  /*
  Variable que contiene el nombre de la casa para buscar los personajes
  */
  public nameHouse: string;

  /*
  Ordenamiento de la tabla
  */
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  /*
  Paginador de la tabla
  */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
   * Constructor de la clase
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  constructor(
    private personagesService: PersonagesService
  ) {
    // Se incializa la primera casa por defecto
    this.nameHouse = 'slytherin';
  }

  ngOnInit() {
    // Se cargan los personajes por la casa indicada
    this.loadPersonagesByHouse();
  }

  /**
   * Metodo que busca los personages de a cuerdo a la casa filtrada
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public async loadPersonagesByHouse() {
    await this.personagesService
      .getPersonagesByHouse(this.nameHouse)
      .then((response) => {

        // Se obtinen los datos de los personajes y se almacenan en el arreglo de personajes
        this.personages = response;

        // Se configura la tabla de la interfaz
        // Columnas a mostrarse en la tabla
        this.displayedColumns = ['name', 'patronus', 'age', 'image'];
        // Objeto que estructura la informacion de la tabla
        this.dataSource = new MatTableDataSource<Person>(this.personages);
        // Paginador de la tabla
        this.dataSource.paginator = this.paginator;
        // Datos que se van a mostrar en la tabla
        this.dataSource.data = this.personages;
        // Ordenamiento de la tabla
        this.dataSource.sort = this.sort;
      })
      .catch((error) => {
        console.error('Error: ', error);
      });
  }

  /**
   * Metodo que realiza la busqueda en la tabla
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public filterTable(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
