import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Person } from '../../entities/person';
import { TeachersService } from '../../services/teachers.service';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {

  /*
  Arreglo de profesores obtenidos en la api
  */
  public teachers: Array<Person>;

  /*
  Arreglo para indicar las columnas de la tabla de la interfaz
  */
  public displayedColumns: string[];

  /*
  Objeto que contiene la informacion de datos y configuracion de la tabla
  */
  public dataSource: MatTableDataSource<Person>;

  /*
  Ordenamiento de la tabla
  */
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  /*
  Paginador de la tabla
  */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /**
   * Constructor de la clase
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  constructor(
    private teachersService: TeachersService
  ) {
  }

  ngOnInit() {
    // Se cargan los profesores
    this.loadTeachers();
  }

  /**
   * Metodo que busca los profesores
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public async loadTeachers() {
    await this.teachersService
      .getTeachers()
      .then((response) => {

        // Se obtienen los profesores
        this.teachers = response;

        // Se configura la tabla de la interfaz
        // Columnas a mostrarse en la tabla
        this.displayedColumns = ['name', 'patronus', 'age', 'image'];
        // Objeto que estructura la informacion de la tabla
        this.dataSource = new MatTableDataSource<Person>(this.teachers);
        // Paginador de la tabla
        this.dataSource.paginator = this.paginator;
        // Datos que se van a mostrar en la tabla
        this.dataSource.data = this.teachers;
        // Ordenamiento de la tabla
        this.dataSource.sort = this.sort;
      })
      .catch((error) => {
        console.error('Error: ', error);
      });
  }

  /**
   * Metodo que realiza la busqueda en la tabla
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public filterTable(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
