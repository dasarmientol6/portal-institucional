import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Person } from '../../entities/person';
import { StudentsService } from '../../services/students.service';

declare const jQuery;
declare const Snackbar;

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {

  /*
  Arreglo de estudiantes obtenidos en la api
  */
  public students: Array<Person>;

  /*
  Arreglo para indicar las columnas de la tabla de la interfaz
  */
  public displayedColumns: string[]

  /*
  Objeto que contiene la informacion de datos y configuracion de la tabla
  */
  public dataSource: MatTableDataSource<Person>;

  /*
  Objeto que contiene la informacion de datos y configuracion de la tabla de los nuevos estudiantes
  */
  public dataSourceNewStudents: MatTableDataSource<Person>;

  /*
  Objeto usado para guardar un nuevo estudiante
  */
  public student: Person;

  /*
  Arreglo de nuevos estudiantes
  */
  public newStudents: Array<Person>;

  /*
  Formulario para validar el guardado de un estudiante
  */
  public formStudent: FormGroup;

  /*
  Ordenamiento de la tabla
  */
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  /*
  Paginador de la tabla
  */
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /*
  Modal de guardado de un estudiante
  */
  @ViewChild('studentModal') studentModal: ElementRef;

  /*
  Modal de los estudiantes nuevos
  */
  @ViewChild('newStudentsModal') newStudentsModal: ElementRef;

  /**
   * Constructor de la clase
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  constructor(
    private studentsService: StudentsService
  ) {

    // Se instancia el formulario para validar el guardado de un estudiante
    this.formStudent = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      gender: new FormControl('', [Validators.required]),
      species: new FormControl('', [Validators.required, Validators.minLength(3)]),
      house: new FormControl('', [Validators.required])
    });

    // Se instancia el estudiante a ser guardado
    this.student = new Person();
    // Se inicializa el arreglo de nuevos estudiantes
    this.newStudents = [];
  }

  ngOnInit() {
    // Se obtienen todos los estudiantes
    this.newStudents = JSON.parse(localStorage.getItem('students'));

    // Se valida que existan datos de nuevos estudiantes para ser mostrados en una tabla
    if (this.newStudents.length > 0) {
      // Se crean las opciones de la tabla para mostrase en la interfaz
        // Columnas a mostrarse en la tabla
        this.displayedColumns = ['name', 'patronus', 'age', 'image'];
        // Objeto que estructura la informacion de la tabla
        this.dataSourceNewStudents = new MatTableDataSource<Person>(this.newStudents);
        // Paginador de la tabla
        this.dataSourceNewStudents.paginator = this.paginator;
        // Datos que se van a mostrar en la tabla
        this.dataSourceNewStudents.data = this.newStudents;
        // Ordenamiento de la tabla
        this.dataSourceNewStudents.sort = this.sort;
    }

    // Carga de la api a todos los estudiantes
    this.loadStudents();
  }

  /**
   * Metodo que obtiene todos los estudiantes
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public async loadStudents() {
    await this.studentsService
      .getStudents()
      .then((response) => {
        // Se agrega la informacion en el arreglo de estudiantes
        this.students = response;

        // Se crean las opciones de la tabla para mostrase en la interfaz
        // Columnas a mostrarse en la tabla
        this.displayedColumns = ['name', 'patronus', 'age', 'image'];
        // Objeto que estructura la informacion de la tabla
        this.dataSource = new MatTableDataSource<Person>(this.students);
        // Paginador de la tabla
        this.dataSource.paginator = this.paginator;
        // Datos que se van a mostrar en la tabla
        this.dataSource.data = this.students;
        // Ordenamiento de la tabla
        this.dataSource.sort = this.sort;
      })
      .catch((error) => {
        console.error('Error: ', error);
      });
  }

  /**
   * Metodo que abre el modal de registro de estudiantes
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public openStudentModal(): void {
    jQuery(this.studentModal.nativeElement).modal('show');
  }

  /**
   * Metodo que obtiene todos los estudiantes nuevos
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public openNewStudentModal(): void {
    jQuery(this.newStudentsModal.nativeElement).modal('show');
  }

  /**
   * Metodo que realiza la busqueda en la tabla
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public filterTable(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /**
   * Metodo que guarda la informacion de un estudiante
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  saveStudent() {
    let text: string = '';
    let backgroundColor: string = '';

    // Se valida el formulario para ser guardado en local storage
    if (this.formStudent.status === 'VALID') {

      // Se llenan los campos del estudiante en el objeto
      this.student.name = this.formStudent.value.name;
      this.student.gender = this.formStudent.value.gender;
      this.student.species = this.formStudent.value.species;
      this.student.house = this.formStudent.value.house;

      // Se guarda el estudiante en el arreglo de nuevos estudiantes
      this.newStudents.push(this.student);

      // Se guarda el arreglo de estudiantes nuevos en el local storage
      localStorage.setItem('students', JSON.stringify(this.newStudents));

      // Se cierra el modal
      jQuery(this.studentModal.nativeElement).modal('hide');

      // Se indica el mensaje y el color del snackbar para mostrar al usuario la informacion de su accion
      text = 'The student has been successfully saved';
      backgroundColor = '#8dbf42';
    } else {
      // Se indica el mensaje y el color del snackbar para mostrar al usuario la informacion de su accion
      text = 'The student has not been saved';
      backgroundColor = '#e7515a';
    }

    // Se crea un mensaje para informar el estado de guardado del estudiante
    Snackbar.show({
      text,
      pos: 'top-center',
      actionTextColor: '#fff',
      backgroundColor,
      duration: 2000,
      showAction: false
    });

    // Se cargan los estudiantes
    this.newStudents = JSON.parse(localStorage.getItem('students'));
  }
}
