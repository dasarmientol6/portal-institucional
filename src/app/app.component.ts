import { Component } from '@angular/core';
import { Router } from '@angular/router';

declare const jQuery: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'portal-institucional';

  /**
   * Constructor de la clase
   *
   * @author Diego Sarmiento - Nov. 07, 2021
   */
  constructor(
    private router: Router,
  ) { }

  /**
   * Metodo para cambiar de pagina en la app
   *
   * @param event Evento donde se obtiene el numero de la pagina
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public changeTab(event) {
    switch (event.index) {
      // Dependiendo la tab seleccionada, se redirige al componente
      case 0:
        this.router.navigateByUrl('/personages');
        break;
      case 1:
        this.router.navigateByUrl('/students');
        break;
      case 2:
        this.router.navigateByUrl('/teachers');
        break;
    }
  }

}
