export class Wand {
    public wood: string;
    public core: string;
    public length: string;

    constructor() {
        this.wood = null;
        this.core = null;
        this.length = null;
    }
}
