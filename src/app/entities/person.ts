import { Wand } from './wand';

export class Person {

    public actor: string;
    public alive: boolean;
    public alternate_actors: Array<string>;
    public alternate_names: Array<string>;
    public ancestry: string;
    public dateOfBirth: string;
    public eyeColour: string;
    public gender: string;
    public hairColour: string;
    public hogwartsStaff: boolean;
    public hogwartsStudent: boolean;
    public house: string;
    public image: string;
    public name: string;
    public patronus: string;
    public species: string;
    public wand: Wand;
    public wizard: boolean;
    public yearOfBirth: number;

    constructor() {
        this.actor = null;
        this.alive = null;
        this.alternate_actors = null;
        this.alternate_names = null;
        this.ancestry = null;
        this.dateOfBirth = null;
        this.eyeColour = null;
        this.gender = null;
        this.hairColour = null;
        this.hogwartsStaff = null;
        this.hogwartsStudent = null;
        this.house = null;
        this.image = null;
        this.name = null;
        this.patronus = null;
        this.species = null;
        this.wand = new Wand();
        this.wizard = null;
        this.yearOfBirth = null;
    }
}
