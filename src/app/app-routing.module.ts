import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PersonagesComponent } from './pages/personages/personages.component';
import { StudentsComponent } from './pages/students/students.component';
import { TeachersComponent } from './pages/teachers/teachers.component';

// Rutas de los modulos para redireccionamiento
const routes: Routes = [
  { path: 'personages', component: PersonagesComponent },
  { path: 'students', component: StudentsComponent },
  { path: 'teachers', component: TeachersComponent },
  { path: '', redirectTo: '/personages', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
