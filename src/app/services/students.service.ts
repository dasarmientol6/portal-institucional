import { Person } from './../entities/person';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  /**
   * Constructor de la clase
   *
   * @param http Servicio para enviar la peticion por http
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Metodo que obtiene los estudiantes
   *
   * @returns Arreglo con estudiantes
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public getStudents(): Promise<Array<Person>> {
    return this.http.get<Array<Person>>(environment.api.base + '/students').toPromise();
  }
}
