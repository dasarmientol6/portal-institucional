import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';
import { Person } from '../entities/person';

@Injectable({
  providedIn: 'root'
})
export class PersonagesService {

  /**
   * Constructor de la clase
   *
   * @param http Servicio para enviar la peticion por http
   */
  constructor(
    private http: HttpClient
  ) { }

  /**
   * Metodo  que obtiene los personajes por el tipo de casa
   *
   * @param nameHouse Nombre de la casa para buscar los personajes
   * @returns Arreglo de personajes
   *
   * @author Diego Sarmiento - Nov, 07-2021
   */
  public getPersonagesByHouse(nameHouse: string): Promise<Array<Person>> {
    return this.http.get<Array<Person>>(environment.api.base + '/house/' + nameHouse).toPromise();
  }
}
